<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="alternate" href="atom.xml" type="application/atom+xml" title="Atom">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/login.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <title>Custom Authenication</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css">
</head>

<body>

    <section class="ftco-section">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-md-7 col-lg-5">
                    <div class="wrap">
                        

                        <div class="img d-flex justify-content-center align-items-center" style="background-image: url({{ asset('assets/images/bg-1.jpg') }});">
                            <h3 class="mb-4">Registration</h3>
                        </div>
                        <div class="login-wrap p-4 p-md-5">
                            

                            <form action="{{ route('register-user') }}" method="POST" class="signin-form">
                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif

                                @if (Session::has('fail'))
                                    <div class="alert alert-danger">{{ Session::get('fail') }}</div>
                                @endif

                                @csrf
                                <div class="form-group my-2">
                                    <label for="name">Full Name</label>
                                    <input type="text" class="form-control" placeholder="Enter full name"
                                        name="name" value="{{ old('name') }}">
                                    <span class="text-danger">
                                        @error('name')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>


                                <div class="form-group my-2">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" placeholder="Enter email" name="email"
                                        value="{{ old('email') }}">
                                    <span class="text-danger">
                                        @error('email')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>


                                <div class="form-group my-2">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" placeholder="Enter password"
                                        name="password" value="">
                                    <span class="text-danger">
                                        @error('password')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-block btn-primary  my-2" type="submit">Register</button>

                                </div>
                                <div class="form-group d-md-flex">
                                    <div class="w-50 text-left">
                                        <label class="checkbox-wrap checkbox-primary mb-0">Remember Me
                                            <input type="checkbox" checked>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="w-50 text-md-right">
                                        <a href="#">Forgot Password</a>
                                    </div>
                                </div>
                                <br>
                                <a href="login">Already Register !! Login Here.</a>

                            </form>
                            {{-- <p class="text-center">Not a member? <a data-toggle="tab" href="#signup">Sign Up</a></p> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="{{ asset('assers/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assers/js/main.js') }}"></script>
    <script src="{{ asset('assers/js/popper.js') }}"></script>
    <script src="{{ asset('assers/js/bootstrap.min.js') }}"></script>

</body>

</html>
